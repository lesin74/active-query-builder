'use strict';

import { sliders } from './sliders';
import { popupCallbackShow } from './modal';
import { popupContactUs } from './modal';

const body = document.querySelector('body');
const nav = document.querySelector('#nav');
const headMenuIndex = document.querySelectorAll('.menu__show');
const tooltipInner = document.querySelectorAll('.tooltip__inner');
const tooltipLink = document.querySelectorAll('.tooltip__link');
const tooltipContent = document.querySelectorAll('.tooltip__content');
const subcontent = document.querySelectorAll('.subcontent');
const subcontentWrapper = document.querySelectorAll('.subcontent__wrapper');
const tooltipMenu = document.querySelectorAll('.tooltip__menu');
const menuMobile = document.querySelector('.burger__menu');
const primaryNav = document.querySelector('.primary-nav');
const hoverUl = document.querySelector('.nav__container');
const menuShow = 980;
let baseHeight = tooltipMenu.clientHeight - 9;
const videoPopup = document.querySelector('.header__board_video');
const headerPointer = document.querySelector('.header__pointer');
const youtubeVideo = document.querySelector('.header__iframe');
let div = document.createElement('div');
const tabsTitleItem = document.querySelectorAll('.tabs__title_item');
const tabsContent = document.querySelectorAll('.tabs__content');
const navHeight = 76;
const leftInfo = document.querySelector('.left__info');
const rightInfo = document.querySelector('.right__info');
const infoContent = document.querySelector('.info__content > img');
const helpBg = document.querySelector('.help');
const started = document.querySelector('.started');
const tabsSelectItem = document.querySelector('.tabs__select_item');
const navButtonCall = document.querySelector('.nav__button_call');
const contactUs = document.querySelector('.contact__us');
const levelTabsTitle = document.querySelectorAll('.level__tabs_title');
const levelTabsItem = document.querySelectorAll('.level__tabs_item');

//мобильное меню
menuMobile.addEventListener('click', () => {
    menuMobile.classList.toggle('burger__menu--active');
    primaryNav.classList.toggle('active');
    tooltipMenu.forEach((items) => {
        items.classList.remove('tooltip__menu_show');
    });
});

//смена бэграунда хедера при скролле
window.addEventListener('scroll', (event) => {
    if (navHeight >= headerPointer.getBoundingClientRect().top) {
        nav.classList.add('nav__bg--active');
    } else {
        nav.classList.remove('nav__bg--active');
    }
});

if (navButtonCall) {
    navButtonCall.addEventListener('click', popupCallbackShow);
}
if (contactUs) {
    contactUs.addEventListener('click', popupContactUs);
}

//табы на странице Active Query Builder for End Users
for (let i = 0; i < levelTabsTitle.length; i++) {
    if (levelTabsItem[i].classList.contains('level__tabs_item--active')) {
        levelTabsItem[i].style.maxHeight = levelTabsItem[i].scrollHeight + 'px';
    }
    levelTabsTitle[i].addEventListener('click', (title) => {
        levelTabsTitle[i].classList.toggle('level__tabs_title--active');
        levelTabsItem[i].classList.toggle('level__tabs_item--active');
        if (levelTabsItem[i].classList.contains('level__tabs_item--active')) {
            levelTabsItem[i].style.maxHeight = levelTabsItem[i].scrollHeight + 'px';
        } else {
            levelTabsItem[i].style.maxHeight = null;
        }
    });
}

sliders();

if (window.innerWidth > menuShow) {
    for (let i = 0; i < headMenuIndex.length; i++) {
        headMenuIndex[i].addEventListener('mouseover', (event) => {
            tooltipMenu[i].classList.add('tooltip__menu_show');
            headMenuIndex[i].style.height = baseHeight + 'px';
            headMenuIndex[i].classList.add('menu__show--active');
            subcontent.forEach((elem) => {
                elem.classList.remove('subcontent--show');
            });
            subcontentWrapper.forEach((elem) => {
                elem.classList.remove('subcontent__wrapper--active');
            });
        });

        headMenuIndex[i].addEventListener('mouseout', (event) => {
            tooltipMenu[i].classList.remove('tooltip__menu_show');
            headMenuIndex[i].classList.remove('menu__show--active');
        });

        tooltipMenu[i].addEventListener('mouseover', (event) => {
            tooltipMenu[i].classList.add('tooltip__menu_show');
        });
        tooltipMenu[i].addEventListener('mouseout', (event) => {
            tooltipMenu[i].classList.remove('tooltip__menu_show');
            headMenuIndex[i].classList.remove('menu__show--active');
        });

        tooltipInner.forEach((elem) => {
            elem.addEventListener('mouseover', (event) => {
                let dataMenu = event.target.parentNode.dataset.menu;
                subcontentWrapper.forEach((el) => {
                    let dataSubmenu = el.dataset.submenu;
                    if (dataMenu === dataSubmenu) {
                        el.classList.add('subcontent__wrapper--active');
                        subcontent.forEach((elem) => {
                            elem.classList.add('subcontent--show');
                            let heightTooltipMenu = el.clientHeight;
                            if (baseHeight < heightTooltipMenu) {
                                tooltipMenu[i].style.height = heightTooltipMenu + 'px';
                            } else {
                                tooltipMenu[i].style.height = baseHeight + 'px';
                                el.style.height = baseHeight + 'px';
                            }
                        });
                    } else {
                        return el.classList.remove('subcontent__wrapper--active');
                    }
                });
            });
        });
    }

    hoverUl.addEventListener('mouseover', (event) => {
        hoverUl.classList.add('nav__container--hover');
    });

    hoverUl.addEventListener('mouseout', (event) => {
        hoverUl.classList.remove('nav__container--hover');
    });
}

if (window.innerWidth <= menuShow) {
    tooltipInner.forEach((elem) => {
        elem.addEventListener('click', (event) => {
            event.target.classList.toggle('tooltip__inner--active');
            let dataMenu = event.target.parentNode.dataset.menu;
            subcontentWrapper.forEach((el) => {
                let dataSubmenu = el.dataset.submenu;
                if (dataMenu === dataSubmenu) {
                    el.classList.toggle('subcontent__wrapper--active');
                    subcontent.forEach((el) => {
                        el.classList.add('subcontent--show');
                    });
                }
            });
        });
    });
}

if (videoPopup) {
    videoPopup.addEventListener('click', (event) => {
        body.appendChild(div);
        div.classList.add('video__bg');
        youtubeVideo.classList.add('header__frame--active');
        if (div) {
            div.addEventListener('click', (e) => {
                youtubeVideo.classList.remove('header__frame--active');
                div.remove();
            });
        }
    });
}

tabsTitleItem.forEach((item) => {
    item.addEventListener('click', (event) => {
        tabsTitleItem.forEach((e) => {
            e.classList.remove('tabs__title_item--active');
        });
        item.classList.add('tabs__title_item--active');
        tabsContent.forEach((content) => {
            if (item.dataset.tabs == content.dataset.tabs) {
                tabsContent.forEach((e) => {
                    e.classList.remove('tabs__content--active');
                });
                content.classList.add('tabs__content--active');
            }
        });
    });
});

if (tabsSelectItem) {
    tabsSelectItem.addEventListener('change', (event) => {
        tabsContent.forEach((content) => {
            if (tabsSelectItem.value == content.dataset.tabs) {
                tabsContent.forEach((e) => {
                    e.classList.remove('tabs__content--active');
                });
                content.classList.add('tabs__content--active');
            }
        });
    });
}

function positionBlock() {
    let width = window.innerWidth;
    switch (true) {
        case width < 960 && width > 639:
            if (leftInfo || rightInfo) {
                leftInfo.style.left = infoContent.getBoundingClientRect().left + 'px';
                rightInfo.style.right = infoContent.getBoundingClientRect().x + 28 + 'px';
            }
            if (helpBg) {
                helpBg.classList.remove('help__mobile');
            }

            if (started) {
                started.classList.add('started__mobile');
            }
            break;

        case width < 639 && width > 479:
            if (leftInfo || rightInfo) {
                leftInfo.style.left = infoContent.getBoundingClientRect().left - 10 + 'px';
                rightInfo.style.right = infoContent.getBoundingClientRect().x + 10 + 'px';
            }
            if (helpBg) {
                helpBg.classList.add('help__mobile');
            }
            if (started) {
                started.classList.add('started__mobile');
            }
            break;

        case width < 479:
            if (helpBg) {
                helpBg.classList.add('help__mobile');
            }
            if (started) {
                started.classList.add('started__mobile');
            }
            break;

        default:
            if (leftInfo || rightInfo) {
                leftInfo.style.left = 20 + 'px';
                rightInfo.style.right = 20 + 'px';
            }
    }
}

positionBlock();

window.addEventListener('resize', (event) => {
    positionBlock();
});
