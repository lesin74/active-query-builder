'use strict';

const bodyModal = document.querySelector('body');
const modal = document.createElement('div');

bodyModal.appendChild(modal);
modal.classList.add('popup');

export function popupCallbackShow(event) {
    bodyModal.classList.add('body__modal--active');
    modal.classList.add('popup--show');
    event.preventDefault();

    //массив с странами для селекта
    const countries = [
        { label: 'Russia', id: 'rus', code: '+7' },
        { label: 'United States', id: 'usa', code: '+1' },
        { label: 'United Kingdom', id: 'gbr', code: '+44' },
        { label: 'Germany', id: 'deu', code: '+49' },
        { label: 'Italy', id: 'ita', code: '+39' },
        { label: 'Australia', id: 'aus', code: '+61' },
        { label: 'Brazil', id: 'bra', code: '+55' },
        { label: 'Argentina', id: 'arg', code: '+54' },
    ];

    //генерация модалки
    modal.innerHTML = `
    <div class="popup__modal">
        <div class="popup__title">Request a callback</div>
        <div class="data__sent">Спасибо! Данные успешно отправлены</div>
        <form action="" class="popup__form">
            <input type="text" class="popup__input_name input__error" name="name" placeholder="Name" />
            <span class="span__error">Обязательное поле</span>
            <input type="text" class="popup__input_email input__error" name="email" placeholder="Your e-mail" />
            <div class="dropdown dropdown__country input__error">
                <div class="dropdown__wrapper">
                    <span class="dropdown__name_country" data-name-code="rus">rus</span>
                    <span class="dropdown__code_phone" data-code="+7" data-phone-mask="+7(999) 999-9999">+7</span>
                </div>
                <div class="dropdown__items">
                </div>
                <input type="tel" class="dropdown__country_number" name="phone" data-code="+7" data-phone-mask="+7(999) 999-99-99" placeholder="99-999-9999" />
            </div>
            <span class="span__error">Пожалуйста, заполните все обязательные поля</span>
            <textarea
                name="message"
                id=""
                cols="30"
                rows="10"
                placeholder="What time is convenient to call you back? Anything else?..."
            ></textarea>
            <div class="textarea__error">Пожалуйста, заполните все обязательные поля</div>
            <button type="submit" class="popup__btn">Please call me back</button>
        </form>
        <span class="popup__subtitle">We'll be glad to help you.</span>
    </div>
    `;

    const dropdownWrapper = document.querySelector('.dropdown__wrapper');
    const dropdownItemLabel = document.querySelector('.dropdown__name_country');
    const dropdownItemSpan = document.querySelector('.dropdown__code_phone');
    const dropdownItems = document.querySelector('.dropdown__items');
    const dropdownCountryNumber = document.querySelector('.dropdown__country_number');

    //генерация списка стран из массива countries
    const dropdownDiv = countries
        .map((i) => {
            return `<div class="dropdown__item">
        <label class="dropdown__item_label" data-name-code="${i.id}">${i.label}</label>
        <span class="dropdown__item_span" data-code="${i.code}" data-phone-mask="${i.code}(999) 999-9999">${i.code}</span>
    </div>`;
        })
        .join(' ');

    dropdownItems.insertAdjacentHTML('afterbegin', dropdownDiv);

    const dropdownItem = document.querySelectorAll('.dropdown__item');

    //закрытие модалки
    modal.addEventListener('click', (e) => {
        if (e.target.classList.contains('popup--show')) {
            bodyModal.classList.remove('body__modal--active');
            modal.classList.remove('popup--show');
            modal.innerHTML = '';
        }
    });

    //открытие/закрытие селекта
    dropdownWrapper.addEventListener('click', (event) => {
        dropdownItems.classList.toggle('dropdown__items--active');
    });

    dropdownCountryNumber.addEventListener('click', (event) => {
        if (dropdownItems.classList.contains('dropdown__items--active')) {
            dropdownItems.classList.remove('dropdown__items--active');
        }
    });

    //выбор страны и передача дата-атрибутов в input
    dropdownItem.forEach((item) => {
        item.addEventListener('click', (event) => {
            let codePhone = item.children[1].innerText;
            let dataPhoneMask = item.children[1].dataset.phoneMask;
            let dataCode = item.children[1].dataset.code;
            let dataNameCode = item.children[0].dataset.nameCode;
            dropdownItemLabel.innerHTML = dataNameCode;
            dropdownItemSpan.innerHTML = codePhone;
            dropdownCountryNumber.dataset.code = dataCode;
            dropdownCountryNumber.dataset.phoneMask = dataPhoneMask;
            dropdownItems.classList.remove('dropdown__items--active');
        });
    });
}

export function popupContactUs(event) {
    bodyModal.classList.add('body__modal--active');
    modal.classList.add('popup--show');
    event.preventDefault();

    //генерация модалки
    modal.innerHTML = `
    <div class="popup__modal">
        <div class="popup__title">Contact us</div>
        <div class="data__sent">Спасибо! Данные успешно отправлены</div>
        <form action="" class="popup__form">
            <input type="text" class="popup__input_name input__error" name="name" placeholder="Name" />
            <span class="span__error">Обязательное поле</span>
            <input type="text" class="popup__input_email email__contact_us input__error" name="email" placeholder="Email" />
            <span class="span__error">Пожалуйста, заполните все обязательные поля</span>
            <textarea
                name="message"
                id=""
                cols="30"
                rows="10"
                placeholder="How can we help you?"
            ></textarea>
            <div class="popup__checkbox">
                <input type="checkbox" class="popup-checkbox" id="label" name="label" value="yes">
                <label for="label">Send me Active Query Builder news from time to time</label>
            </div>
            <div class="textarea__error">Пожалуйста, заполните все обязательные поля</div>
            <button type="submit" class="popup__btn">Contact me</button>
        </form>
        <span class="popup__subtitle">We'll reach out to you in hours</span>
    </div>
    `;

    //закрытие модалки
    modal.addEventListener('click', (e) => {
        if (e.target.classList.contains('popup--show')) {
            bodyModal.classList.remove('body__modal--active');
            modal.classList.remove('popup--show');
            modal.innerHTML = '';
        }
    });
}
